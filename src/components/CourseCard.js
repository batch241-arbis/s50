import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import {Link} from 'react-router-dom'

function CourseCard({course}) {

  const {name, description, price, _id} = course;
  // const [count, setCount] = useState(30);
  // const [isOpen, setIsOpen] = useState(true);
  // console.log(useState);

  // function enroll() {
  //   setCount(count - 1);
  // }

  // useEffect(() => {
  //     if(count === 0) {
  //       setIsOpen(false);
  //       document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
  //     }
  //   }, [count]);

  // console.log({name})
  return (
    <Card>
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text>
          Description:<br/ >
          {description}
        </Card.Text>
        <Card.Text>
          Price:<br/ >
          PHP {price}
        </Card.Text>
        {/*<Button variant="primary">Enroll</Button>*/}
        {/*<Button className="bg-primary" id={'btn-enroll-' + _id} onClick={enroll}>Enroll</Button>*/}
        <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
      </Card.Body>
    </Card>
  );
}

export default CourseCard;