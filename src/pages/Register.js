import { Navigate } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import userContext from '../UserContext'
import Swal from 'sweetalert2';
import {useNavigate} from 'react-router-dom';

export default function Register() {
	const navigate = useNavigate();

	const {user, setUser} = useContext(userContext);

	const[email, setEmail] = useState('');
	const[fName, setFName] = useState('');
	const[lName, setLName] = useState('');
	const[number, setNumber] = useState('');
	const[password1, setPassword1] = useState('');
	const[password2, setPassword2] = useState('');
	const[isActive, setIsActive] = useState(false);

	function registerUser(e) {
		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: fName,
				lastName: lName,
				email: email,
				mobileNo: number,
				password: password1
			})
		})
		.then(res => res.json())
		.then(data => {
			if( data.registerSuccess !== false){
					Swal.fire({
						title: "Registration successful",
	 					icon: "success",
	 					text: "Welcome to Zuitt!"
				})
					navigate("/login");
				} else {
	  				Swal.fire({
	  					title: "Duplicate email found",
	  					icon: "error",
	  					text: "Please provide a different email."
				})
			}
		});

		// setEmail('');
		// setPassword('');
	}

	useEffect(() => {
		if((password1 == password2) && (password1 != "") && (email != "") && ((fName != "") && (lName != "")) && ((number != "") && (number.length == 11))){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [fName, lName, email, number, password1, password2])

    return (
    	(user.email != null) ?
    	<Navigate to ="/courses"/>
    	:
        <Form onSubmit={(e)=> registerUser(e)}>
            <Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
	                type="text" 
	                placeholder="Enter first name" 
	                value={fName}
	                onChange={e => setFName(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
	                type="text" 
	                placeholder="Enter last name" 
	                value={lName}
	                onChange={e => setLName(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                value={email}
	                onChange={e => setEmail(e.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="number">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
	                type="number" 
	                placeholder="Enter mobile number" 
	                value={number}
	                onChange={e => setNumber(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                value={password1}
	                onChange={e => setPassword1(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password" 
	                value={password2}
	                onChange={e => setPassword2(e.target.value)}
	                required
                />
            </Form.Group>

            { isActive ?
            <Button variant="primary" type="submit" id="submitBtn" className="mt-3">
            	Submit
            </Button>
            :
            <Button variant="danger" type="submit" id="submitBtn" className="mt-3" disabled>
            	Submit
            </Button>
        	}
            
        </Form>
    )

}