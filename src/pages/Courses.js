import {useState, useEffect} from 'react'

// import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses(){
	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/activecourses`)
		.then(res => res.json())
		.then(data => {

			setCourses(data.map(course => {
				return (
					<CourseCard key={course._id} course={course}/>
				)
			}))

		})
	}, [])

	return (
				<>
				{courses}
				</>
			)

	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} course ={course}/>
	// 	)
	// })
	// return (
	// 	<>
	// 	{courses}
	// 	</>
	// )
}