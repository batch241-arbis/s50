import { Form, Button } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';

import {Navigate} from 'react-router-dom';

import userContext from '../UserContext'

import Swal from 'sweetalert2';

export default function Login() {

	const {user, setUser} = useContext(userContext);

	const[email, setEmail] = useState('');
	const[password, setPassword] = useState('');
	const[isActive, setIsActive] = useState(false);

	function loginUser(e) {
		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access)
					console.log("test yes")
					Swal.fire({
						title: "Successfully enrolled",
	 					icon: "success",
	 					text: "You have successfully enrolled for this course."
				})
				} else {
					console.log("test no")
	  				Swal.fire({
	  					title: "Something went wrong",
	  					icon: "error",
	  					text: "Please try again."
				})
			}
		});
		// localStorage.setItem("email", email);
		// setUser({email: localStorage.getItem('email')})
		setEmail('');
		setPassword('');
		
		// alert('You are now logged in!');
	}

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data.id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {
		if(password !== "" && email !== ""){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])


    return (
    	(user.id !== null) ?
    	<Navigate to ="/courses"/>
    	:
        <Form onSubmit={(e)=> loginUser(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                value={email}
	                onChange={e => setEmail(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                value={password}
	                onChange={e => setPassword(e.target.value)}
	                required
                />
            </Form.Group >
            { isActive ?
            <Button variant="success" type="submit" id="submitBtn" className="mt-3">
            	Submit
            </Button>
            :
            <Button variant="success" type="submit" id="submitBtn" className="mt-3" disabled>
            	Submit
            </Button>
        	}
            
        </Form>
    )

}