// import {Link} from 'react-router-dom';

// export default function () {
// 	return (
// 		<div>
//       <h1>
//       	Page not found.
//       </h1>
//       <p>
//       	Go back to <Link to="/">Homepage</Link>.
//       </p>
//     </div>
// 	)
// }

import Banner from '../components/Banner';

export default function Error() {
	const data = {
		title: "Error 404 - Page Not Found",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back to Home"
	}
	return (
		<Banner data={data}/>
	)
}