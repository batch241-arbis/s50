import Banner from '../components/Banner'
import Highlights from '../components/Highlights'	


export default function Home() {
	const data = {
		title: "Welcome",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back to Home"
	}
	return (
		<>
			<Banner data={data} />
     		<Highlights />
     		
		</>
	)
}
